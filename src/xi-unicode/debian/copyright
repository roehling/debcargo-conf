Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xi-unicode
Upstream-Contact: Raph Levien <raph.levien@gmail.com>
Source: https://github.com/google/xi-editor

Files: *
Copyright:
 2016-2020 Google Inc.
 2020-2022 Raph Levien <raph.levien@gmail.com>
License: Apache-2.0

Files: ./tests/LineBreakTest.txt
Copyright: 2017 Unicode®, Inc.
License: Unicode

Files: debian/*
Copyright:
 2018-2022 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2018 kpcyrd <git@rxv.cc>
 2022 Alexander Kjäll <alexander.kjall@gmail.com>
License: Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: Unicode
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of the Unicode data files and any associated documentation (the "Data
 Files") or Unicode software and any associated documentation (the
 "Software") to deal in the Data Files or Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, and/or sell copies of the Data Files or Software, and
 to permit persons to whom the Data Files or Software are furnished to do
 so, provided that (a) the above copyright notice(s) and this permission
 notice appear with all copies of the Data Files or Software, (b) both the
 above copyright notice(s) and this permission notice appear in associated
 documentation, and (c) there is clear notice in each modified Data File or
 in the Software as well as in the documentation associated with the Data
 File(s) or Software that the data or software has been modified.
 .
 THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR
 CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THE DATA FILES OR SOFTWARE.
 .
 Except as contained in this notice, the name of a copyright holder shall
 not be used in advertising or otherwise to promote the sale, use or other
 dealings in these Data Files or Software without prior written
 authorization of the copyright holder.
 .
 Unicode and the Unicode logo are trademarks of Unicode, Inc., and may be
 registered in some jurisdictions. All other trademarks and registered
 trademarks mentioned herein are the property of their respective owners.
